# Spring Boot and Sonar Cloud

Proof of Concept

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=pisw.portafolio.docente.epis-spring-boot-sonar-cloud&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=pisw.portafolio.docente.epis-spring-boot-sonar-cloud)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=pisw.portafolio.docente.epis-spring-boot-sonar-cloud&metric=bugs)](https://sonarcloud.io/summary/new_code?id=pisw.portafolio.docente.epis-spring-boot-sonar-cloud)

[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=pisw.portafolio.docente.epis-spring-boot-sonar-cloud&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=pisw.portafolio.docente.epis-spring-boot-sonar-cloud)

[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=pisw.portafolio.docente.epis-spring-boot-sonar-cloud&metric=coverage)](https://sonarcloud.io/summary/new_code?id=pisw.portafolio.docente.epis-spring-boot-sonar-cloud)

[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=pisw.portafolio.docente.epis-spring-boot-sonar-cloud&metric=duplicated_lines_density)](https://sonarcloud.io/summary/new_code?id=pisw.portafolio.docente.epis-spring-boot-sonar-cloud)

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-black.svg)](https://sonarcloud.io/summary/new_code?id=pisw.portafolio.docente.epis-spring-boot-sonar-cloud)

